This is my first discord bot
It is designed to pull card art off of mtg visual spoilers and put them into a discord channel

The basic functionality is there but it will definitely be updated with better functionality and more features when I have time.

Enjoy!


DISCLAIMER!!
-------------
I dont run my bot often unless I'm testing it but feel free to put your own token in the .env file and run it for yourself!
--------------

In the env file there is a place for a BOT TOKEN and spot for a CHANNEL ID
If you diecide to plug this into your own bot you NEED to fill those out or it will not work.

--------------------
The cards.csv file is currently full and the program will not pull any cards that have already been put in the file, if you want it to pull everything that has come out this year and is going to come out this year just delete the file and replace it with an empty one.
