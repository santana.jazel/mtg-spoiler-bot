# This program is designed to scrape the main spoiler page of www.magicspoiler.com for the URLs to upcoming sets,
# then pull the card art from their respective pages and output it
import requests
import csv
import datetime
import discord
import os
from dotenv import load_dotenv
from bs4 import BeautifulSoup
from discord.ext import commands, tasks
from discord.ext.commands import Greedy, Context
from typing import Optional, Literal

card_file = 'Cards.csv'
URLs_dic = {}
cards_dic = {}
new_cards_list = []
mtg_spoil_url = "https://www.magicspoiler.com/mtg-spoilers/"
load_dotenv()

# bot tokens and related variables
intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix='>', intents=intents)
BOT_TOKEN = os.getenv('TOKEN')
CHANNEL_ID = int(os.getenv('MTG_SPOILERS_ID'))


# Declaring variables to enable scraping of the main page of www.magicspoiler.com
def get_set_elements(url):
    year = str(datetime.datetime.now().year)
    page_0 = requests.get(url)
    soup_0 = BeautifulSoup(page_0.content, "html.parser")
    results_0 = soup_0.find(class_="upcoming-sets")
    new_sets = results_0.find_all(
        "p", string=lambda text: year in text
    )
    return (
        a_element.parent.parent.parent for a_element in new_sets
    )


def set_urls_dic(set_elements):
    for sets in set_elements:
        set_title = sets.find("div", class_="upcoming-set").next_element.text
        URLs_dic.update({set_title: sets["href"]})


# Imports preexisting cards from csv files
def read_cards(cards):
    if os.stat(cards).st_size == 0:
        print("File is initialized")
    else:
        with open(cards, mode="r") as f:
            data = csv.reader(f, delimiter="_")
            for row in data:
                card = MTGCard(*row)
                cards_dic.update({str(card.name): card})


# defines a class to hold information from the magic cards scraped
class MTGCard:
    def __init__(self, name, art_loc, is_new):
        self.name = name
        self.art_loc = art_loc
        self.is_new = is_new

    def __str__(self):
        return f"{self.name} has card art located at {self.art_loc}"

    def __iter__(self):
        return iter([self.name, self.art_loc, self.is_new])


# takes a URL as input and returns all card art spoilers from that page to a .csv file
def pull_art_from(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    results = soup.find(id="MS Spoilers M")
    card_arts = results.find_all("article", class_="set-card-2")

    for card_art in card_arts:
        link = card_art.find('img')['src']
        card_name = card_art.find('a')['title']
        card = MTGCard(card_name, link, True)
        cards_dic.update({str(card.name): card})


def find_new():
    for key in cards_dic.keys():
        if cards_dic[key].is_new is True:
            new_cards_list.append(cards_dic[key])


def write_cards():
    with open(card_file, 'a') as stream:
        for key in cards_dic.keys():
            if cards_dic[key].is_new is True:
                cards_dic[key].is_new = False
                stream.write("%s_%s_%s\n" % (cards_dic[key].name, cards_dic[key].art_loc, cards_dic[key].is_new))


@bot.event
async def on_ready():
    print(f'Logged in as {bot.user} (ID: {bot.user.id})')
    print('------')
    spoil_me.start()
    channel = bot.get_channel(CHANNEL_ID)
    await channel.send("bot is ready")


@tasks.loop(hours=24)
async def spoil_me():
    channel = bot.get_channel(CHANNEL_ID)
    set_urls_dic(get_set_elements(mtg_spoil_url))  # pushes all set URLs into a dictionary of URLs
    for vals in URLs_dic.values():  # loops over the URL dict and calls the art puller on each page
        pull_art_from(vals)

    read_cards(card_file)  # reads cards from file into the dictionary, will override all previously pushed cards with is_new = false
    find_new()  # looks for cards with the is_new == True property and pushes them to a list
    if len(new_cards_list) > 0:
        await channel.send(f"**New Cards Just Dropped!!**")

        for item in new_cards_list:
            await channel.send(f"{item.name}\n{item.art_loc}")
        write_cards()  # updates csv files with spoilers
    else:
        await channel.send(f"**No New Cards Today** :(")
    new_cards_list.clear()


@bot.tree.command()
async def help(interaction: discord.Interaction) -> None:
    """Help, Help I'm Being Repressed"""
    await interaction.response.send_message(f'You want help?\nListen I am a bot not a therapist.....I cannot help you', ephemeral=True)


@bot.command()
@commands.guild_only()
@commands.is_owner()
async def sync(
  ctx: Context, guilds: Greedy[discord.Object], spec: Optional[Literal["~", "*", "^"]] = None) -> None:
    if not guilds:
        if spec == "~":
            synced = await ctx.bot.tree.sync(guild=ctx.guild)
        elif spec == "*":
            ctx.bot.tree.copy_global_to(guild=ctx.guild)
            synced = await ctx.bot.tree.sync(guild=ctx.guild)
        elif spec == "^":
            ctx.bot.tree.clear_commands(guild=ctx.guild)
            await ctx.bot.tree.sync(guild=ctx.guild)
            synced = []
        else:
            synced = await ctx.bot.tree.sync()

        await ctx.send(
            f"Synced {len(synced)} commands {'globally' if spec is None else 'to the current guild.'}"
        )
        return

    ret = 0
    for guild in guilds:
        try:
            await ctx.bot.tree.sync(guild=guild)
        except discord.HTTPException:
            pass
        else:
            ret += 1

    await ctx.send(f"Synced the tree to {ret}/{len(guilds)}.")

bot.run(BOT_TOKEN)  # starts the listener for the bot now that all dictionaries are up-to-date.
